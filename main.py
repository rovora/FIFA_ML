import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import sqlite3
import tensorflow
from keras.models import Sequential
from keras.layers import Dense, Activation

pd.set_option('display.max_columns', 50)
db = 'database.sqlite'

conn = sqlite3.connect(db)
tables = pd.read_sql("""SELECT *
                        FROM sqlite_master
                        WHERE type='table';""", conn)
tables = pd.read_sql_query("SELECT * FROM sqlite_master WHERE type='table';", conn )
countries = pd.read_sql("SELECT * FROM Country;", conn)
leagues = pd.read_sql("""SELECT * FROM League 
                LEFT OUTER JOIN Country ON League.country_id = Country.id;""", conn)

matches = pd.read_sql("""SELECT country_id, league_id, season, stage, date,
                home_team_goal, away_team_goal, home_team_api_id, away_team_api_id
                FROM Match;""", conn)
matches['year'] = matches.date.str[:4]
matches.head()
matches.columns
team_att = pd.read_sql("SELECT * from Team_Attributes;", conn)
team_att['year'] = team_att.date.str[:4]
mh = pd.merge(matches, team_att, left_on = ['home_team_api_id', 'year'], right_on = ['team_api_id', 'year'], how = 'left' )


mha = pd.merge(mh, team_att, left_on = ['away_team_api_id', 'year'], 
               right_on = ['team_api_id', 'year'], how = 'left', 
               suffixes = ('_home', '_away' ))


mha['wtl'] = 0
mha.wtl[mha.home_team_goal > mha.away_team_goal] = 2
mha.wtl[mha.home_team_goal == mha.away_team_goal] = 1

# check the dimensions of mha
mha.shape
#check the column names
mha.columns
# check the type of the columns
mha.dtypes
# check the basic stats of the numeric columns
# the count is an indication of the number of valid values
mha.describe()
mha.to_csv("fifa_2008to2016_features.csv")

print(round(mha['wtl'].corr(mha['buildUpPlaySpeed_home']),3), " Home: Buildup Play Speed ")
print(round(mha['wtl'].corr(mha['buildUpPlaySpeed_away']),3), " Away: Buildup Play Speed ")
print(round(mha['wtl'].corr(mha['buildUpPlayDribbling_home']), 3), " Home: Buildup Play Dribbling ")
print(round(mha['wtl'].corr(mha['buildUpPlayDribbling_away']), 3), " Away: Buildup Play Dribbling ")
print(round(mha['wtl'].corr(mha['buildUpPlayPassing_home']), 3), " Home: Buildup Play Passing ")
print(round(mha['wtl'].corr(mha['buildUpPlayPassing_away']), 3), " Away: Buildup Play Passing ")

print(round(mha['wtl'].corr(mha['chanceCreationPassing_home']), 3), " Home: Chance Creation Passing")
print(round(mha['wtl'].corr(mha['chanceCreationPassing_away']), 3), " Away: Chance Creation Passing")
print(round(mha['wtl'].corr(mha['chanceCreationCrossing_home']), 3), " Home: Chance Creation Crossing")
print(round(mha['wtl'].corr(mha['chanceCreationCrossing_away']), 3), " Away: Chance Creation Crossing")
print(round(mha['wtl'].corr(mha['chanceCreationShooting_home']), 3), " Home: Chance Creation Shooting")
print(round(mha['wtl'].corr(mha['chanceCreationShooting_away']), 3), " Away: Chance Creation Shooting")

print(round(mha['wtl'].corr(mha['defencePressure_home']), 3), " Home: Defence Pressure")
print(round(mha['wtl'].corr(mha['defencePressure_away']), 3), " Away: Defence Pressure")
print(round(mha['wtl'].corr(mha['defenceAggression_home']), 3), " Home: Defence Aggression")
print(round(mha['wtl'].corr(mha['defenceAggression_away']), 3), " Away: Defence Aggression")
print(round(mha['wtl'].corr(mha['defenceTeamWidth_home']), 3), " Home: Defence Team Width")
print(round(mha['wtl'].corr(mha['defenceTeamWidth_away']), 3), " Away: Defence Team Width")



fe = mha[['buildUpPlaySpeed_home', 'buildUpPlaySpeed_away',
          'buildUpPlayDribbling_home', 'buildUpPlayDribbling_away',
          'buildUpPlayPassing_home', 'buildUpPlayPassing_away',
          'chanceCreationPassing_home', 'chanceCreationPassing_away',
          'chanceCreationCrossing_home', 'chanceCreationCrossing_away',
          'chanceCreationShooting_home', 'chanceCreationShooting_away',
          'defencePressure_home', 'defencePressure_away',
          'defenceAggression_home', 'defenceAggression_away',
          'defenceTeamWidth_home', 'defenceTeamWidth_away', 'wtl']]

fe = mha[['buildUpPlaySpeed_home', 'buildUpPlaySpeed_away',
          'buildUpPlayPassing_home', 'buildUpPlayPassing_away',
          'chanceCreationPassing_home', 'chanceCreationPassing_away',
          'chanceCreationCrossing_home', 'chanceCreationCrossing_away',
          'chanceCreationShooting_home', 'chanceCreationShooting_away',
          'defencePressure_home', 'defencePressure_away',
          'defenceAggression_home', 'defenceAggression_away',
          'defenceTeamWidth_home', 'defenceTeamWidth_away', 'wtl']]


fe_in = fe.iloc[:].dropna()
fe_in.describe()
fe_in.to_csv("fifa_2008to2016_clean.csv")

fe = fe_in.sample(frac = 1).reset_index(drop = True)
label = fe['wtl']
hot_labels = keras.utils.to_categorical(label, num_classes = 3)
fe = fe.drop(['wtl'], axis = 1)

#fe = pd.read_csv("fifa_2008to2016_clean.csv")
#fe.shape

model = Sequential()
model.add(Dense(32, activation = 'relu', input_dim = 16))
model.add(Dense(3, activation = 'softmax'))
model.compile(optimizer = 'rmsprop', loss = 'binary_crossentropy', metrics = ['accuracy'])


hist = model.fit(fe, hot_labels, validation_split = 0.2, epochs = 100)
plt.plot(hist.history['acc'])
plt.plot(hist.history['val_acc'])
plt.show()