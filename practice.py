# -*- coding: utf-8 -*-
"""
Created on Mon May 28 15:10:07 2018

@author: rohan
"""

import pandas
from sklearn.datasets import load_breast_cancer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import classification_report, confusion_matrix

cancer = load_breast_cancer()
#Get the data from the cancer dataset
x =  cancer['data']
y = cancer['target']

#Split the data into train and test using preprocessing tools
x_train, x_test, y_train, y_test = train_test_split(x,y)
scaler = StandardScaler()
scaler.fit(x_train)

#Data transform and store back into test and train datasets
x_train = scaler.transform(x_train)
x_test = scaler.transform(x_test)

#Get the MLP thing
mlp = MLPClassifier(hidden_layer_sizes=(10))
mlp.fit(x_train, y_train)

predictions = mlp.predict(x_test)
print(classification_report(y_test,predictions))
print(confusion_matrix(y_test,predictions))