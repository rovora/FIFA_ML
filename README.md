# Fifa Match Outcome Predictor 


## Getting The Data
Data for the European soccer leagues from 2009-2015 can be fond in this [SQLite3 database created by Hugo Mathien](https://www.kaggle.com/hugomathien/soccer/data) on Kaggle.
<br />[Dima Rudov provides a nice introduction](https://www.kaggle.com/dimarudov/data-analysis-using-sql) (and SQL!) to the database.
<br /> You can download DB Browser for SQLite [here.](http://sqlitebrowser.org/)


## Setting up the Environment


## Useful Links

Tutorials, etc: <br />
[A Beginner's Guide to Neural Networks with Python and SciKit Learn (Jose Portilla) ](https://www.kdnuggets.com/2016/10/beginners-guide-neural-networks-python-scikit-learn.html)
<br />

<br />
Papers:<br />
[Predicting Soccer Match Results in the English Premier League (Ulmer & Fernandez)](http://cs229.stanford.edu/proj2014/Ben%20Ulmer,%20Matt%20Fernandez,%20Predicting%20Soccer%20Results%20in%20the%20English%20Premier%20League.pdf)
<br />
[Football Match Results Prediction Using Artifical Neural Networks: The Case of Iran Pro League (Arabzad, Araghi, Soheil & Ghofrani)](https://www.researchgate.net/publication/267026589_Football_Match_Results_Prediction_Using_Artificial_Neural_Networks_The_Case_of_Iran_Pro_League)
<br />